# Gitlab

Installiert eine [Gitlab-Instanz](https://docs.gitlab.com/omnibus/).

## Features

* Installation von Gitlab
* Installation eines Backup-Cronjobs und das dazugehörige Backup-Script
* Installation eines Recovery-Scripts